An interview on distributing computation across the edge to cloud compute continuum was given by Elli Kartsakli in the 68th edition of the HiPEAC info Magazine, published in January 2023 (pages 22-23). PROXIMITY is one of the relevant projects mentioned in this interview. 

